# Music Compressor

simple music compressor that uses lame

<ul> 
To use you must:<br />
    <li>Have lame installed</li>
    <li>Have getopt installed</li>
</ul>


All arguments are optional (because they have defaults):<br/><br/>

-o|--disregard to compress all ( *.mp3 ) files in the Parent Directory, default is false<br/>
-c|--include-compressed to compress songs previously compressed with the same quality, default is false<br/>
-q|--quality	quality_in_kbps sets the quality of the compressed songs, must be between 16kbps and 320kbps the number only! default is 128<br/>
-p|--parent-directory	absolute_path_to_where_music_is sets the directory from which the program will work, must be absolute! default is /home/$USER/Music<br/>
-d|--directory relative_path_to_dir_from_from_parent sets the directory where the compressed songs will go, must be relative to parent directory, default is 'compressed_QUALITY'<br/>
-e|--exclude-list absolute_path_to_file sets the location of the list of songs user want excluded from the compression, must be a text file!, must be absolute! default is /dev/null<br/>
-l|--compressed-list relative_from_compressed_dir sets the location of the file to store a list of all compressed songs with same quality, is a relative path from compressed directory, default is music_compressed_list_QUALITY.txt<br/>
-h|--help displays the help menu
